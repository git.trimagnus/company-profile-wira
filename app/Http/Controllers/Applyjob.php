<?php

namespace App\Http\Controllers;
use Illuminate\Http\Request;
use app\Post;
use Mail; //Jangan lupa import Mail di paling atas
use redirect;
use image;
use storage;
use app\Mailfile;

class Applyjob extends Controller
{   
    public function getcontactJob()
    {
        return view('job/enginer');
    }

    public function postcontactJob(Request $request)
    {
        $this->validate($request,[
            'email' => 'required|email',
            'subject' => 'min:3',
            'message' => 'min:10'
        ]);

        $data = array(
            'email' =>$request->email,
            'subject' =>$request->subject,
            'bodyMessage' =>$request->message,
            'a_file' =>$request->a_file
        );

        mail::send('emailjob', $data, function($message) use ($data){
            // $message->to('razipahmad0@gmail.com');
            $message->to('info@wiraenergi.co.id');
            $message->subject($data['subject']);
            $message->from('donotreply@wiraenergi.com', 'Wira Energi');
            // $message->attach($data['a_file']->getRealpath(), array(
            //     'as' => 'a_file.' . $data['a_file']->getClientOriginalExtension(),
            //     'mime' => $data['a_file']->getMimeType())
            //     );
            $message->attach($data['a_file']);
        // var_dump($message);die;

            // return back()->with('alert-success','Emali Berhasil Dikirim :)');
        });
        session::flash('success',"Emali Berhasil Dikirim :)");
        return redirect::to('/contactJob');
        // var_dump($data);die;


        // try{
        //     Mail::send('emailjob', [
        //         'nama' => $request->name,
        //         'pesan' => $request->pesan,
        //         'email' => $request->email,
        //         'cv' => $request->cv
        //     ], function ($message) use ($request)
        //     {
        //         $message->subject($request->judul);
        //         $message->from('donotreply@wiraenergi.com', 'Wira Energi');
        //         $message->attach($request['cv']->getRealpath(), array(
        //             'as' => 'cv.' . $request['cv']->getClientOriginalExtention()
        //             // 'mime' => $request['cv']->getMimeType()
        //         )
        //         );
        //         // $message->to('info@wiraenergi.co.id');
        //         // $message->to('razipahmad0@gmail.com');
        //         $message->to('razip@trimagnus.com');
        //     });
        //     return back('/Career')->with('alert-success','Emali Berhasil Dikirim :)');
        // }
        // catch (Exception $e){
        //     // return response (['status' => false,'errors' => $e->getMessage()]);
        //     return response ('alert-failed','Emali Gaga; Dikirim :(');
        // }
    }
}
