@extends('template.index')
@section('content')

    <!--================ start banner Area =================-->
    <section class="service-banner-area" id="electricity">
        <div class="container">
            <div class="row justify-content-end fullscreen">
                <div class="col-lg-7 col-md-12 d-flex fullscreen">
                    <div class="text">
                        <h1>
                            Electricity
                        </h1>
                    </div>
                </div>
                <div class="col-lg-5 col-md-12 no-padding fullscreen">
                </div>
            </div>
        </div>
    </section>
    <!--================ End banner Area =================-->
    <!--================ Start provide tab Area =================-->
    <section>
        <div class="container cntn-service" style="margin-top: 20px">
            <div class="row">
                <div class="col-lg-6 col-md-6" style="margin: auto;">
                    <div class="title">
                        We builds & helps you save the electricity and steam’s bill Power, heat and cooling supply (CHPC)
                    </div><br>
                    <p style="text-align: justify;">
                        Using an absorption or an adsorption chiller, CHP systems can also create cooling energy with their generated heat. This functionality could be used for air conditioning in an office building, for example.
                    </p>
                    <p style="text-align: justify;">
                        The supply of cooling energy plays an important role not only for building climate control – it can be utilized in many different areas, as well as including process cooling in manufacturing, for food refrigeration or for ambient cooling in temperature-sensitive areas (e.g. data centers).
                    </p>
                </div>
                <div class="col-lg-6 col-md-6" style="text-align: center;">
                    <img src="{{asset('template/img/img-electricity.png')}}" style="width: 80%;height: auto;">
                </div>
            </div>
        </div>
    </section>
    <!--================ End provide tab Area =================-->
<!-- <script>
    $(function(){
        $('.topnav .dropdown a').each(function(){
            $(this).addClass('active');
        });
    });
</script> -->
@endsection