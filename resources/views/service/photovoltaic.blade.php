@extends('template.index')
@section('content')

    <!--================ start banner Area =================-->
    <section class="service-banner-area" id="photovoltaic">
        <div class="container">
            <div class="row justify-content-end fullscreen">
                <div class="col-lg-7 col-md-12 d-flex fullscreen">
                    <div class="text">
                        <h1>
                            Photovoltaic
                        </h1>
                    </div>
                </div>
                <div class="col-lg-5 col-md-12 no-padding fullscreen">
                </div>
            </div>
        </div>
    </section>
    <!--================ End banner Area =================-->
    <!--================ Start provide tab Area =================-->
    <section>
        <div class="container cntn-service" style="margin-top: 20px">
            <div class="row">
                <div class="col-lg-6 col-md-6">
                    <div class="title">
                        WIRA ENERGI Solar System
                    </div><br>
                    <p style="text-align: justify;">
                        From applications engineering support through product delivery and commissioning to site monitoring and field services, you can count on us - we'll be there with you.
                        Challenge us. Our tagline is "Value." - we think value creation starts at the early stages of design with thoughtful application engineering. Then, we aim to make product implementation as easy and fast as possible. Our goal is to be on site with you for every commissioning event for smooth turn-on and approvals.
                        Our service team is located in each region and ready to roll. We know speed matters as does personal attention to detail. These are the ways we aim to create "Value" for you.
                    </p><br>
                    <div class="title">
                        Full Cycle Life Support
                    </div>
                </div>
                <div class="col-lg-6 col-md-6" style="text-align: center;">
                    <img src="{{asset('template/img/service/photovoltaict/pvproducts.png')}}" style="width: 80%;height: auto;">
                </div>
            </div>
        </div>
    </section>
    <!--================ End provide tab Area =================-->
@endsection