@extends('template.index')
@section('content')

    <!--================ start banner Area =================-->
    <section class="service-banner-area" id="waster-water-treatment">
        <div class="container">
            <div class="row justify-content-end fullscreen">
                <div class="col-lg-7 col-md-12 d-flex fullscreen">
                    <div class="text">
                        <h1>
                            Waste Water Treatment
                        </h1>
                    </div>
                </div>
                <div class="col-lg-5 col-md-12 no-padding fullscreen">
                </div>
            </div>
        </div>
    </section>
    <!--================ End banner Area =================-->
    <!--================ Start provide tab Area =================-->
    <section>
        <div class="container cntn-service" style="margin-top: 20px">
            <div class="row">
                <div class="col-lg-6 col-md-6">
                    <div class="title">
                        WE charges you per Liter of your waste water or that you have used, let us do the rest.
                    </div><br>
                    <p style="text-align: justify;">
                        Clean, safe water is vital for every day life. Water is essential for health, hygiene and the productivity of our community. The water treatment process may vary slightly at different locations, depending on the technology of the plant and the water it needs to process, but the basic principles are largely the same. The charge of the waste treatment will be include as a facilities or services to the end users and it could be put as a fix value (Dumping Fee for solid waste).
                    </p>
                </div>
                <div class="col-lg-6 col-md-6" style="text-align: center;">
                    <img src="{{asset('template/img/service/waste-water/14.png')}}" style="width: 80%;height: auto;">
                </div>
            </div>
        </div>
    </section>
    <!--================ End provide tab Area =================-->
@endsection