@extends('template.index')
@section('content')

    <!--================ start banner Area =================-->
    <section class="service-banner-area" id="natural">
        <div class="container">
            <div class="row justify-content-end fullscreen">
                <div class="col-lg-7 col-md-12 d-flex fullscreen">
                    <div class="text">
                        <h1>
                            Natural Gas
                        </h1>
                    </div>
                </div>
                <div class="col-lg-5 col-md-12 no-padding fullscreen">
                </div>
            </div>
        </div>
    </section>
    <!--================ End banner Area =================-->
    <!--================ Start provide tab Area =================-->
    <section>
        <div class="container cntn-service" style="margin-top: 20px">
            <div class="row">
                <div class="col-lg-6 col-md-6" style="margin: ;">
                    <div class="title">
                        Thru pipeline, CNG and LNG
                    </div><br>
                    <p style="text-align: justify;">
                        WE offers many advantages by using the natural gas through a distribution pipeline within the area of industrial estates/parks, commercial and residential. replacing LPG using NG for restaurant in your existing malls will get some benefit in cost, safety and efficiency. CNG (Compressed Natural Gas) could be used before the online pipeline is connected.
                    </p><br>
                    <div class="title">
                        Let us manage NG supply at your Malls.
                    </div><br>
                    <p>
                        Our services include:
                        <ul style="list-style-type:disc;margin-left: 30px;">
                            <li>Inspection of the standard & quality of the existing gas pipeline or engineering & design a new installation.</li>
                            <li>Investment in pipeline modification and online pipeline connection including all required accessories.</li>
                            <li>Adjustment all appliances using NG at all tenants.</li>
                            <li>Guarantee of the NG supply.</li>
                            <li>Handling all complaints and compensating claim from your tenants.</li>
                        </ul>
                        Our leading products are:
                        <ul style="list-style-type: disc;margin-left: 30px">
                            <li>ISO tank container, storage tank and trailer in cryogenic field for LNG.</li>
                            <li>LNG filling skid and LNG filling station.</li>
                            <li>CNG storage cylinder, CNG trailer and CNG refilling station.</li>
                        </ul>
                        </p>
                    </a>
                </div>
                <div class="col-lg-6 col-md-6" style="text-align: center;">
                   <!--  <img src="{{asset('template/img/img-natural-gas.png')}}" style="width: 80%;height: auto;">
 -->
                    <img src="{{asset('template/img/service/gas/IMG_8289.jpg')}}" style="width: 80%;height: auto">

                    <img src="{{asset('template/img/service/gas/IMG_8305.jpg')}}" style="width: 80%;height: auto;margin-top: 30px;">

                    <img src="{{asset('template/img/service/gas/IMG_8346.jpg')}}" style="width: 80%;height: auto;margin-top: 30px;">
                    <!-- <div class="row" style="margin-top: 30px;">
                        <div class="col-lg-4 col-md-4 col-sm-4">
                            <img src="{{asset('template/img/no image.png')}}" style="width: 80%;height: auto;">
                        </div>

                        <div class="col-lg-4 col-md-4 col-sm-4">
                            <img src="{{asset('template/img/no image.png')}}" style="width: 80%;height: auto;">
                        </div>

                        <div class="col-lg-4 col-md-4 col-sm-4">
                            <img src="{{asset('template/img/no image.png')}}" style="width: 80%;height: auto;">
                        </div>
                    </div> -->
                </div>
            </div>
        </div>
    </section>
    <!--================ End provide tab Area =================-->
@endsection