@extends('template.index')
@section('content')

    <!--================ start banner Area =================-->
    <section class="service-banner-area" id="high-speed-diesel">
        <div class="container">
            <div class="row justify-content-end fullscreen">
                <div class="col-lg-7 col-md-12 d-flex fullscreen">
                    <div class="text">
                        <h1>
                            High Speed Diesel
                        </h1>
                    </div>
                </div>
                <div class="col-lg-5 col-md-12 no-padding fullscreen">
                </div>
            </div>
        </div>
    </section>
    <!--================ End banner Area =================-->
    <!--================ Start provide tab Area =================-->
    <section>
        <div class="container cntn-service" style="margin-top: 20px">
            <div class="row">
                <div class="col-lg-6 col-md-6" style="margin: auto;">
                    <div class="title">
                        Trusted diesel fuel supplier
                    </div><br>
                    <p style="text-align: justify;">
                        We have the competitive edge on supply and pricing by offering our product with assured quality, quantity and availability. With sufficient transport facilities, WE guarantees to deliver supply on time. WE is supplying HSD (High Speed Diesel) in partial or continuing order to industries with a very competitive price.
                    </p>
                    <p style="text-align: justify;">
                        WE are working together in distributing High Speed ​​diesel (HSD) in the JABODETABEK region with PT. Indomobil Prima Energi (IPE) Which has a license "SKP" (Surat Keterangan Penyalur Minyak Bahan Bakar Umum) from PT. ExxonMobil Lubricants Indonesia (EMLI).
                    </p>
                </div>
                <div class="col-lg-6 col-md-6" style="text-align: center;">
                    <img src="{{asset('template/img/PastedGraphic-2.jpg')}}" style="width: 80%;height: auto;">
                    <!-- <img src="{{asset('template/img/service/hsd/HSD.jpeg')}}" style="width: 80%;height: auto;"> -->
                </div>
            </div>
        </div>
    </section>
    <!--================ End provide tab Area =================-->
    @endsection