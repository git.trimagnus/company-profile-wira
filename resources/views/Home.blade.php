@extends('template.index')
@section('content')

    <!--================ start banner Area =================-->
    <section class="home-banner-area">
        <div class="container">
            <div class="row justify-content-end fullscreen">
                <div class="col-lg-7 col-md-0 col-sm-0 home-banner-left d-flex fullscreen align-items-center hiden">
                </div>
                <div class="col-lg-5 col-md-12 col-sm-0 no-padding home-banner-right d-flex fullscreen align-items-end">
                    <div style="margin: auto;" id="phone-right">
                        <h1 class="">
                            We Are <span>WE!</span><br>
                            WE build TODAY FOR<br>
                            <span>TOMORROW</span>
                        </h1>
                        <p class="mx-auto mb-40" style="color: green;">
                            Safe, reliable, efficient and <br>
                            environmentally sound manner
                        </p>

                        <!-- <div class="d-flex align-items-center mt-60">
                            <a id="play-home-video" class="video-play-button" href="https://www.youtube.com/watch?v=vParh5wE-tM">
                                <span></span>
                            </a>
                            <div class="watch_video">
                                <h5>Watch Live Demo</h5>
                                <p>You will love our execution</p>
                            </div>
                        </div> -->
                    </div>
                </div>
            </div>
        </div>
        <div class="tabs">
            <ul class="nav nav-pills mb-3" id="pills-tab" role="tablist">
                <li class="nav-item">
                    <a class="nav-link active" id="pills-home-tab" data-toggle="tab" href="#Natural-Gas" role="tab"><span>Natural Gas</span></a>
                </li>
                <li class="nav-item">
                    <a class="nav-link" id="pills-profile-tab" data-toggle="tab" href="#High-Speed-Diesel" role="tab"><span>High Speed Diesel</span></a>
                </li>
                <li class="nav-item">
                    <a class="nav-link" id="pills-contact-tab" data-toggle="tab" href="#Lot" role="tab"><span>IoT</span></a>
                </li>
            </ul>
        </div>
    </section>
    <!--================ End banner Area =================-->

    <!--================ Start Content tab Area =================-->
    <div class="container-fluid" style="background: #fafafa">
        <div class="tab-content" id="pills-tabContent" style="margin-top: -16px">
              <!-- cntn 1 -->
              <div class="tab-pane fade show active" id="Natural-Gas" role="tabpanel" aria-labelledby="pills-home-tab">
                  <div class="row">
                      <div class="col-lg-5 col-md-12 col-sm-12 mid">
                          <div class="center">
                              <span style="font-size: 60px;color: #429C46">Natural Gas</span><br>
                          </div>
                          <div class="bottom" id="PC">
                              <a href="{{ url('/Service/Natural-Gas') }}">
                                  <img src="{{ asset('template/img/icon.png') }}" style="width: auto;height: auto;"><span style="font-size: 18px;padding-left: 20px;"> More Info</span>
                              </a>
                          </div>
                      </div>
                      <div class="col-lg-7 cl-md-12 col-sm-12">
                          <img src="{{ asset('template/img/PastedGraphic-1.jpg')}}" style="width: 100%;height: auto;" alt="">
                      </div>
                      <div class="bottom" id="HP">
                              <a href="{{ url('/Service/Natural-Gas') }}">
                                  <img src="{{ asset('template/img/icon.png') }}" style="width: auto;height: auto;"><span style="font-size: 18px;padding-left: 20px;"> More Info</span>
                              </a>
                          </div>
                  </div>
              </div>
              <!-- cntn 2 -->
              <div class="tab-pane fade" id="High-Speed-Diesel" role="tabpanel" aria-labelledby="pills-profile-tab">
                  <div class="row">
                      <div class="col-lg-5 col-md-5 mid">
                          <div class="center">
                              <span style="font-size: 40px;color: #429C46">High Speed Diesel</span><br>
                          </div>
                          <div class="bottom" id="PC">
                              <a href="{{ url('/Service/High-Speed-Diesel') }}">
                                  <img src="{{ asset('template/img/icon.png') }}" style="width: auto;height: auto;"><span style="font-size: 18px;padding-left: 20px;"> More Info</span>
                              </a>
                          </div>
                        </div>
                      <div class="col-lg-7 cl-md-7">
                          <img src="{{ asset('template/img/PastedGraphic-2.jpg')}}" style="width: 100%;height: auto;" alt="">
                      </div>
                      <div class="bottom" id="HP">
                              <a href="{{ url('/Service/High-Speed-Diesel') }}">
                                  <img src="{{ asset('template/img/icon.png') }}" style="width: auto;height: auto;"><span style="font-size: 18px;padding-left: 20px;"> More Info</span>
                              </a>
                          </div>
                  </div>
              </div>
              <!-- cntn 3 -->
              <div class="tab-pane fade" id="Lot" role="tabpanel" aria-labelledby="pills-contact-tab">
                  <div class="row">
                      <div class="col-lg-5 col-md-5 mid">
                          <div class="center">
                              <span style="font-size: 40px;color: #429C46">Internet of Things</span><br>
                          </div>
                          <div class="bottom" id="PC">
                              <!--<a href="{{ url('/Service/Internet-Of-Things') }}">
                                  <img src="{{ asset('template/img/icon.png') }}" style="width: auto;height: auto;"><span style="font-size: 18px;padding-left: 20px;"> More Info</span>
                              </a>-->
                              <a href="https://weiots.io" target="_blank">
                                  <img src="{{ asset('template/img/icon.png') }}" style="width: auto;height: auto;"><span style="font-size: 18px;padding-left: 20px;"> More Info</span>
                              </a>
                          </div>
                      </div>
                      <div class="col-lg-7 cl-md-7">
                          <!-- <div class="d-flex align-items-center mt-60-img">
                            <a id="play-home-video" class="video-play-button" href="{{ asset('template/img/WE Convenient Meter Video.MP4') }}">
                                <span></span>
                            </a>
                          </div> -->
                          <a href="https://youtu.be/C0QwWZvH97E" target="_blank">
                            <img src="{{ asset('template/img/poster2.png')}}" style="width: 100%;height: auto;">
                          </a>
                          <!-- <video width="100%" height="auto" poster="{{ asset('template/img/poster2.png')}}" controls controls>
                            <source src="{{ asset('template/img/WE Convenient Meter Video.MP4')}}" type="video/mp4">
                            <source src="movie.ogg" type="video/ogg">
                          Your browser does not support the video tag.
                          </video> -->
                      </div>
                      <div class="bottom" id="HP">
                              <a href="{{ url('/Service/Internet-Of-Things') }}">
                                  <img src="{{ asset('template/img/icon.png') }}" style="width: auto;height: auto;"><span style="font-size: 18px;padding-left: 20px;"> More Info</span>
                              </a>
                          </div>
              </div>
            </div>
    </div>
</div>
    <!--================ End Content tab Area =================-->
    <!--================ Start provide tab Area =================-->
    <section>
        <div class="container" style="margin: 100px auto">
            <div class="row">
                <div class="col-lg-5 col-md-5" style="text-align: center;">
                    <img src="{{asset('template/img/we-sign.png')}}" style="width: 80%;height: auto;">
                </div>
                <div class="col-lg-6 col-md-6" style="margin: auto;">
                    <h4>WE provide the most reliable and economical energy and water solutions to our present an future customers</h4><br>
                    <p>
                        WE provides the most reliable and economical energy and water solutions to our present and future customers in Residential, Commercial and Industrial Area. Partner in managing your energy requirement and improving your value of the business, saving the energy cost without or minor spending any additional
                    </p><br>
                    <a href="{{ url('/About-Us') }}">
                        <img src="{{ asset('template/img/icon.png') }}" style="width: auto;height: auto;"><span style="font-size: 18px;padding-left: 20px;"> More Info</span>
                    </a>
                </div>
                <div class="col-lg-1 col-md-1">

                </div>
            </div>
        </div>
    </section>
    <!--================ End provide tab Area =================-->
    <!--================ Start Portfolio Service Area =================-->
    <section class="portfolio-area">
        <div class="container-fluid">
            <div class="row align-items-center service"><!--
                <div class="col-lg-1 col-md-1 col-sm-0"></div> -->
                <div class="col-lg-3 col-md-3 col-sm-12">
                    <div class="portfolio-main">
                        <h1 style="font-family: roboto;color: #429C46;">Service</h1>
<!--                         <a href="#" class="primary-btn">View all Works</a> -->
                    </div>
                </div>
                <div class="col-lg-9 col-md-9">
                    <div class="active-gallery-carousel">
                        <!-- single gallery -->
                        <div class="card-service">
                            <div class="logo">
                                <img src="{{ asset('template/img/service/natural gas.png')}}" style="width: 70%;margin: auto;">

                            </div>
                            <div class="texthead">
                              <h4>Natural Gas</h4>
                            </div>
                            <div class="isi">
                              WE offers many advantages by using the natural gas through a distribution pipeline within the area of industrial estates/parks, commercial and residential
                            </div>
                            <div class="button">
                              <button class="btn btn-default btn-sm" onclick="window.location.href='{{asset('/Service/Natural-Gas')}}'">More Info</button>
                            </div>
                        </div>
                        <!-- single gallery -->
                        <div class="card-service">
                            <div class="logo">
                                <img src="{{ asset('template/img/service/electricity.png')}}" style="width: 70%;margin: auto;">

                            </div>
                            <div class="texthead">
                              <h4>Electricity</h4>
                            </div>
                            <div class="isi">
                              WE builds & helps you save the electricity and steam’s bill with Power, heat and cooling supply (CHPC).
                            </div>
                            <div class="button">
                              <button class="btn btn-default btn-sm" onclick="window.location.href='{{asset('/Service/Electricity')}}'" onclick="window.location.href='{{asset('')}}'">More Info</button>
                            </div>
                        </div>
                        <!-- single gallery -->
                        <div class="card-service">
                            <div class="logo">
                                <img src="{{ asset('template/img/service/Water treatment.png')}}" style="width: 70%;margin: auto;">

                            </div>
                            <div class="texthead">
                              <h4>Water Treatment</h4>
                            </div>
                            <div class="isi">
                              WE charges you per Liter of your waste water or that you have used, let us do the rest.
                            </div>
                            <div class="button">
                              <button class="btn btn-default btn-sm" onclick="window.location.href='{{asset('Service/Water-Treatment')}}'">More Info</button>
                            </div>
                        </div>
                        <!-- single gallery -->
                        <div class="card-service">
                            <div class="logo">
                                <img src="{{ asset('template/img/service/waster water treatment.png')}}" style="width: 70%;margin: auto;">

                            </div>
                            <div class="texthead">
                              <h5>Waste Water Treatment</h5>
                            </div>
                            <div class="isi">
                              Clean, safe water is vital for every day life. Water is essential for health, hygiene and the productivity of our community.
                            </div>
                            <div class="button">
                              <button class="btn btn-default btn-sm" onclick="window.location.href='{{asset('/Service/Waster-Water-Treatment')}}'">More Info</button>
                            </div>
                        </div>
                        <!-- single gallery -->
                        <div class="card-service">
                            <div class="logo">
                                <img src="{{ asset('template/img/service/High speed diesel.png')}}" style="width: 70%;margin: auto;">

                            </div>
                            <div class="texthead">
                              <h4>High Speed Diesel</h4>
                            </div>
                            <div class="isi">
                              We have the competitive edge on supply and pricing by offering our product with assured quality, quantity and availability.
                            </div>
                            <div class="button">
                              <button class="btn btn-default btn-sm" onclick="window.location.href='{{asset('/Service/High-Speed-Diesel')}}'">More Info</button>
                            </div>
                        </div>
                        <!-- single gallery -->
                        <div class="card-service">
                            <div class="logo">
                                <img src="{{ asset('template/img/service/waster to energi.png')}}" style="width: 70%;margin: auto;">

                            </div>
                            <div class="texthead">
                              <h4>Waste to Energi</h4>
                            </div>
                            <div class="isi">
                              Waste is a problem that represents high costs to governments, citizens, companies, and has a strong environmental effect on our planet.
                            </div>
                            <div class="button">
                              <button class="btn btn-default btn-sm" onclick="window.location.href='{{asset('/Service/Waste-To-Energy')}}'">More Info</button>
                            </div>
                        </div>
                        <!-- single gallery -->
                        <div class="card-service">
                            <div class="logo">
                                <img src="{{ asset('template/img/service/Photvoltaic.png')}}" style="width: 70%;margin: auto;">

                            </div>
                            <div class="texthead">
                              <h4>Photovoltaic</h4>
                            </div>
                            <div class="isi">
                              From applications engineering support through product delivery and commissioning to site monitoring and field services, you can count on us - we'll be there with you.
                            </div>
                            <div class="button">
                              <button class="btn btn-default btn-sm" onclick="window.location.href='{{asset('/Service/Photovoltaic')}}'">More Info</button>
                            </div>
                        </div>
                        <!-- single gallery -->
                        <div class="card-service">
                            <div class="logo">
                                <img src="{{ asset('template/img/service/led.png')}}" style="width: 70%;margin: auto;">

                            </div>
                            <div class="texthead">
                              <h4>Internet of Things</h4>
                            </div>
                            <div class="isi">
                             Wira Energi is private utility company that is focusing on utilities supply. We encounter internal difficulties by using traditional utility meter from manual reading,
                            </div>
                            <div class="button">
                              <button class="btn btn-default btn-sm" onclick="window.location.href='{{asset('/Service/Internet-Of-Things')}}'">More Info</button>
                            </div>
                        </div>
                        <!-- <div class="single-gallery">
                            <img class="img-fluid" src="{{ asset('template/img/project/gallery1.jpg') }}" alt="">
                            <div class="gallery-content">
                                <p>Proper Guided Tour</p>
                                <h4>Santorini Island Dream <br>
                                    Holiday and Fun <br>
                                    Package</h4>
                            </div>
                            <div class="light-box">
                                <a href="{{ asset('template/img/project/gallery1.jpg') }}" class="img-popup">
                                    <span class="lnr lnr-link"></span>
                                </a>
                            </div>
                        </div> -->
                    </div>
                </div>
                <div class="col-lg-12 col-md-12 servicectn" style="color: #fff;text-align: center;">
                    <h3 style="font-family: roboto;color: #fff;">We're an energetic and determined team driven by results, committed to helping your business succeed.</h3>
                </div>
            </div>
        </div>
    </section>
    <!--================ End Portfolio Service Area =================-->
    <!--================ Start Contact Us Area =================-->
<!--     <section>
        <div class="container">
            <div class="row">
                <div class="col-lg-8 col-md-8">
                  <form action="{{ url('/sendEmail') }}" method="post" class="contact" style="background: #429C46;padding: 20px;border-radius: 5px;box-shadow: 4px 4px 4px 4px darkgray;">
                      {{ csrf_field() }}
                        <div class="row">
                            <div class="col-lg-6 col-md-6">
                                <div class="form-group has-success">
                                    <label class="fff" for="inputSuccess1">Name</label>
                                    <input type="text" class="form-control green" id="name" name="name">
                                </div>
                            </div>
                            <div class="col-lg-6 col-md-6">
                                <div class="form-group has-success">
                                    <label class="fff" for="">Email</label>
                                    <input type="text" class="form-control green" id="email" name="email">
                                </div>
                            </div>
                            <div class="col-lg-12 col-md-12">
                                <div class="form-group has-success">
                                    <label class="fff" for="">Subject</label>
                                    <input type="text" class="form-control green" id="judul" name="judul">
                                </div>
                            </div>
                            <div class="col-lg-12 col-md-12">
                                <div class="form-group has-success">
                                    <label class="fff" for="">Meesage</label>
                                    <textarea class="form-control green" id="pesan" name="pesan"></textarea>
                                </div>
                            </div>
                            <div class="col-lg-12 col-md-12">
                                @if(\Session::has('alert-failed'))
                                    <div class="alert alert-failed" style="float: left;">
                                        <div>{{Session::get('alert-failed')}}</div>
                                    </div>
                                @endif
                                @if(\Session::has('alert-success'))
                                    <div class="alert alert-success" style="float: left;">
                                        <div>{{Session::get('alert-success')}}</div>
                                    </div>
                                @endif
                                <button type="submit" style="color: #429C46;float: right;background: #fff;" class="btn btn-default btn-sm">Send Message</button>
                            </div>
                        </div>
                    </form>
                </div>
                <div class="col-lg-4 col-md-4" style="margin: auto;">
                    <h5>
                        We'd Love to help you
                    </h5>
                    <h1 style="color: #FFB300;font-size: 70px;">
                        Let's <br>Connect
                    </h1>
                </div>
            </div>
        </div>
    </section> -->
    <!--================ End Contact Us Area =================-->

    @endsection
