@extends('template.index')
@section('content')
    
    <!--================ start banner Area =================-->
<!--     <section class="service-banner-area" id="">
        <div class="container">
            <div class="row justify-content-end fullscreen">
                <div class="col-lg-7 col-md-12 d-flex fullscreen">
                    <div class="text2">
                        <h1>
                            Open Positions
                        </h1><br>
                        <h4>
                            WE cares and nurtures our people
                        </h4>
                    </div>
                </div>
                <div class="col-lg-5 col-md-12 no-padding fullscreen">
                </div>
            </div>
        </div>
      </section> -->
      <!--================ Start Contact Area =================-->
    <section style="margin-top: 10%;">
        <div class="container job">
          <a href="{{url('/Career')}}">
            Back To Career
          </a>
          <h2>Engineer</h2>
          <p>
            Location In Jakarta
          </p>
          <div class="tabs2">
            <ul class="nav nav-pills mb-2" id="pills-tab" role="tablist">
                <li class="nav-item2">
                    <a class="nav-link2 active" id="Role-Overview" data-toggle="tab" href="#overview" role="tab"><span>Role-Overview</span></a>
                </li>
                <li class="nav-item2">
                    <a class="nav-link2" id="Application" data-toggle="tab" href="#application" role="tab"><span>Application</span></a>
                </li>
            </ul>
        </div>
<!-- tabs content -->
        <div class="tab-content" id="pills-tabContent" style="margin-top: -16px">
              <!-- cntn 1 -->
              <div class="tab-pane fade show active" id="overview" role="tabpanel" aria-labelledby="Role-Overview">
                  <div class="row">
                      <div class="col-lg-3 col-md-6 col-sm-12" style="padding:5% 2% 2% 2%;line-height: 1">
                          <p>
                            S1 (Undergraduate)
                          </p>
                          <p>
                            2 Years Experience
                          </p>
                          <p>
                            Good connection in Government
                          </p>
                          <p>
                            Honest and Loyal
                          </p>
                          <p>
                            Responsible
                          </p>
                      </div>
                      <div class="col-lg-9 cl-md-6 col-sm-12" style="padding: 2%;line-height: 1">
                          <p style="font-size: 20px;color: #429C46">
                            Our benefits include
                          </p>
                          <p>
                            BPJS
                          </p>
                          <p>
                            Lucrative Bonus and Commission
                          </p>
                          <p>
                            Work from Anywhere
                          </p>
                          <p>
                            Advancement Opportunity
                          </p>
                          <p>
                            NO Discrimination
                          </p>
                          <p>
                            5 days paid Sick/Leave Days per Year
                          </p>
                      </div>
                  </div>
              </div>
              <!-- cntn 2 -->
              <div class="tab-pane fade" id="application" role="tabpanel" aria-labelledby="Application">
                  <form action="{{ url('/sendEmail') }}" method="post">
                    {{ csrf_field() }}
                    <div class="row">
                      @if(\Session::has('alert-failed'))
                                    <div class="alert alert-failed" style="float: left;color: red;">
                                        <div>{{Session::get('alert-failed')}}</div>
                                    </div>
                                @endif
                                @if(\Session::has('alert-success'))
                                    <div class="alert alert-success" style="float: left;color: #429C46; ">
                                        <div>{{Session::get('alert-success')}}</div>
                                    </div>
                                @endif
                      <div class="col-lg-12 col-md-12 col-sm-12" style="padding: 2%;line-height: 1">
                        <p style="font-size: 20px;color: #429C46">
                          Upload your CV here
                        </p>
                      </div>
                        <div class="col-lg-3 col-md-6 col-sm-12" style="padding:0px 2%;line-height: 1">
                        <div class="form-group">
                            <label>Name</label>
                            <input type="text" class="form-control" id="name" name="name">
                          </div>
                          <div class="form-group">
                            <label>Email</label>
                            <input type="email" class="form-control" id="email" name="email">
                          </div>
                          <div class="form-group">
                            <!-- <label>Subject</label> -->
                            <!-- <input type="text" class="form-control" id="judul" name="judul"> -->
                            <input type="hidden" class="form-control" id="judul" name="judul" value="Job application for Engineer">
                          </div>
                      </div>
                      <div class="col-lg-7 cl-md-7" style="padding:0px 2%;line-height: 1">
                          <!-- <div class="form-group">
                            <label>Select A File To Upload</label>
                            <input type="file" class="form-control" id="a_file" name="a_file">
                          </div> -->
                          <div class="form-group">
                            <label>Message</label>
                            <textarea class="form-control" id="pesan" name="pesan" rows="4"></textarea>
                          </div>
                      </div>
                      <div class="col-lg-12 col-md-12 col-sm-12">
                        <div class="row">
                          <div class="col-lg-2 col-md-2 col-sm-2">
                            <div class="form-group">
                              <button style="color: #429C46;float: left;width: 100%;margin: auto;" class="btn btn-default btn-sm" type="submit">Submit</button>
                              
                            </div>
                          </div>
                          <div class="col-lg-10 col-md-10 col-sm-10">
                              <button style="visibility: hidden;" class="btn btn-light btn-sm" onclick="Location.href='mailto:info@wiraenergi.co.id?subject=Job application for Engineer'">adf</button>
                              <span>
                                Or Apply via email to <a href="mailto:info@wiraenergi.co.id?subject=Job application for Engineer">: info@wiraenergi.co.id</a>
                              </span>
                          </div>
                        </div>
                    </div>
                  </form>
              </div>
          </div>
        </div>
    </section>
    <!--================ End Contact Area =================-->
    @endsection