<!DOCTYPE html>
<html>
<head>
	<link rel="icon" type="image/png" href="{{ asset('template/img/logo_wira_bulet.png')}}" style="height: 50px">
	<title>Wira Energi</title>
</head>
<body>
@include('template.header')

	@yield('content')

@include('template.footer')
</body>
</html>