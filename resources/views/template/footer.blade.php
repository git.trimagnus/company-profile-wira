<!DOCTYPE html>
<html>
<body>
<!--================ start footer Area  =================-->
    <footer class="footer-area section_gap">
        <div class="container">
            <div class="row footer-top"><!--
                <div class="col-lg-3 col-md-6 col-sm-12">
                    <div class="single-footer-widget">
                        <img src="{{ asset('template/img/logo.png')}}" style="width: 50px;margin-bottom: 14px;"><span class="title"> Wira Energi</span>
                        <p><a target="_blank" href="https://maps.google.com/maps?ll=-6.174758,106.789674&z=15&t=m&hl=id&gl=ID&mapclient=embed&cid=3192040537214324325">Soho Podomoro City (NEO SOHO) Lantai 40 Unit 09, Jl Let. Jend. S. Parman Kav 28 Jakarta Barat 11470 Indonesia</a>
                        </p>
                        <img src="{{ asset('template/img/Facebook.png')}}" style="height: 30px;margin-bottom: 14px;margin:5px;"><img src="{{ asset('template/img/instagram.png')}}" style="height: 30px;margin-bottom: 14px;margin:5px;"><img src="{{ asset('template/img/linkind.png')}}" style="height: 30px;margin-bottom: 14px;margin:5px;"><img src="{{ asset('template/img/youtube.png')}}" style="height: 30px;margin-bottom: 14px;margin:5px;">
                       </div>
                </div>
                <div class="col-lg-3 col-md-6 col-sm-6">
                    <div class="single-footer-widget">
                        <h6>Contact Us</h6>
                        <div class="row">
                            <ul class="col-lg-12 col-md-12 col-sm-12 footer-nav">
                                <li>Phone <a href="tel:+622129181308">: +62 21 2918 1308</a><a href="tel:+622129181309">/09</a></li>
                                <li>Fax <a href="fax:+622129181308">: +62 21 2918 1308</a></li>
                                <li>Email <a href="mailto:info@wiraenergi.co.id?subject=Testing link mail">: info@wiraenergi.co.id</a></li>
                            </ul>
                        </div>
                    </div>
                </div>
                <div class="col-lg-1 col-md-3 col-sm-6">
                    <div class="single-footer-widget">
                        <h6>Company</h6>
                        <p>
                        	<a href="{{ url('/About-Us') }}">About Us</a>
                        </p>
                        <p>
                        	<a href="{{ url('/Gallery') }}">Gallery</a>
                        </p>
                        <p>
                        	<a href="{{ url('/Career') }}">Career</a>
                        </p>
                    </div>
                </div>
                <div class="col-lg-3 col-md-6 col-sm-6">
                    <div class="single-footer-widget">
                        <h6>Service</h6>
                        <div class="row">
                            <ul class="col-lg-5 col-md-5 col-sm-5 footer-nav">
                                <li>
                                   <a href="{{ url('/Service/Natural-Gas') }}">Natural Gas</a>
                                </li>
                                <li>
                                   <a href="{{ url('/Service/Electricity') }}">Electricity</a>
                                </li>
                                <li>
                                    <a href="{{ url('/Service/Photovoltaic') }}">Photovoltaic</a>
                                </li>
                                <li>
                                    <a href="{{ url('/Service/Internet-Of-Things') }}">Internet Of Things (IoT)</a>
                                </li>
                            </ul>
                            <ul class="col-lg-7 col-md-7 col-sm-7 footer-nav">
                                <li>
                                    <a href="{{ url('/Service/Water-Treatment') }}">Water Treatment</a>
                                </li>
                                <li>
                                    <a href="{{ url('/Service/High-Speed-Diesel') }}">High Speed Diesel</a>
                                </li>
                                <li>
                                    <a href="{{ url('/Service/Waste-To-Energy') }}">Waste To Energy</a>
                                </li>

                                <li>
                                    <a href="{{ url('/Service/Waster-Water-Treatment') }}">Waste Water Treatment</a>
                                </li>
                            </ul>
                        </div>
                    </div>
                </div>
                <div class="col-lg-2 col-md-3 col-sm-6">
                    <div class="single-footer-widget">
                        <h6>Product</h6>
                       	<p>
                            <a href="{{ url('/Service/Natural-Gas') }}">Natural Gas</a>
                        </p>
                        <p>
                        	<a href="{{ url('/Service/High-Speed-Diesel') }}">HSD</a>
                        </p>
                        <p>
                            <a href="{{ url('/Service/Internet-Of-Things') }}">IoT</a>
                        </p>

                       </div>
                </div> -->
                <div class="col-lg-6 col-md-6 col-sm-6">
                    <div class="single-footer-widget">
                        <img src="{{ asset('template/img/logo.png')}}" style="width: 50px;margin-bottom: 14px;"><span class="title"> Wira Energi</span>
                        <p><a target="_blank" href="https://maps.google.com/?q=-6.194082869341424,106.70229888964649">Rukan Crown, Blok A No.23 Jl. Green Lake City Boulevard No.10, RT.001/RW.010, Petir, Kec. Cipondoh, Kota Tangerang, Banten 15147 Indonesia</a>
                        </p>
                        <img src="{{ asset('template/img/Facebook.png')}}" style="height: 30px;margin-bottom: 14px;margin:5px;"><img src="{{ asset('template/img/instagram.png')}}" style="height: 30px;margin-bottom: 14px;margin:5px;"><img src="{{ asset('template/img/linkind.png')}}" style="height: 30px;margin-bottom: 14px;margin:5px;"><img src="{{ asset('template/img/youtube.png')}}" style="height: 30px;margin-bottom: 14px;margin:5px;">
                       </div>
                </div>
                <div class="col-lg-6 col-md-6 col-sm-6">
                    <div class="single-footer-widget">
                        <h6>Contact Us</h6>
                        <div class="row">
                            <ul class="col-lg-12 col-md-12 col-sm-12 footer-nav">
                                <li>Phone <a href="tel:+622154333290">: +6221-5433-3290</a><a href="tel:+622154333290">/09</a></li>
                                <!--<li>Fax <a href="fax:+622129181308">: +62 21 2918 1308</a></li>-->
                                <li>Email <a href="mailto:info@wiraenergi.co.id?subject=Testing link mail">: info@wiraenergi.co.id</a></li>
                            </ul>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="footer-bottom d-flex justify-content-between align-items-center flex-wrap">
            <div class="container">
                <div class="row justify-content-between">
                    <div class="col-lg-12">
                       	<span style="font-weight: bold;">Copyright © 2020 Wira Energi. All Rights Reserved</span>
                        <p>Design By Trimagnus</p>
                    </div>
                </div>
            </div>
        </div>
    </footer>
    <!--================ End footer Area  =================-->


    <!-- <script src="{{ asset('template/js/vendor/jquery-2.2.4.min.js') }}"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.11.0/umd/popper.min.js" integrity="sha384-b/U6ypiBEHpOf/4+1nzFpr53nxSS+GLCkfwBdFNTxtclqqenISfwAzpKaMNFNmj4"
     crossorigin="anonymous"></script>
    <script src="{{ asset('template/js/vendor/bootstrap.min.js') }}"></script>
    <script src="{{ asset('template/js/jquery.ajaxchimp.min.js') }}"></script>
    <script src="{{ asset('template/js/jquery.magnific-popup.min.js') }}"></script>
    <script src="{{ asset('template/js/parallax.min.js') }}"></script>
    <script src="{{ asset('template/js/owl.carousel.min.js') }}"></script>
    <script src="{{ asset('template/js/jquery.nice-select.min.js') }}"></script>
    <script src="{{ asset('template/js/jquery.sticky.js') }}"></script>
    <script src="{{ asset('template/js/hexagons.min.js') }}"></script>
    <script src="{{ asset('template/js/jquery.counterup.min.js') }}"></script>
    <script src="{{ asset('template/js/waypoints.min.js') }}"></script>
    <script src="{{ asset('template/js/main.js') }}"></script> -->

</body>
</html>
