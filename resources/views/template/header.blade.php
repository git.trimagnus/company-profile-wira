<!DOCTYPE html>
<html lang="zxx" class="no-js">

<head>
    <!-- Mobile Specific Meta -->
    <meta name="viewport" content="width=device-width, initial-scale=1, minimum-scale=1, shrink-to-fit=no">
    <!-- Favicon-->
    <!-- <link rel="shortcut icon" href="img/fav.png"> -->
    <!-- Author Meta -->
    <meta name="author" content="colorlib">
    <!-- Meta Description -->
    <meta name="description" content="">
    <!-- Meta Keyword -->
    <meta name="keywords" content="">
    <!-- meta character set -->
    <meta charset="UTF-8">
    <!-- Site Title -->
    <title>Wira Energi</title>

    <link href="https://fonts.googleapis.com/css?family=Playfair+Display:700|Roboto:400,400i,500" rel="stylesheet">
    <!--
            CSS
            ============================================= -->
    <link rel="stylesheet" href="{{ asset('template/css/linearicons.css') }}">
<!--     <link rel="stylesheet" href="{{ asset('template/css/font-awesome.min.css') }}"> -->
    <link rel="stylesheet" href="{{ asset('template/fontawesome/css/all.css')}}">
    <link rel="stylesheet" href="{{ asset('template/css/bootstrap.css') }}">
    <link rel="stylesheet" href="{{ asset('template/css/bootstrap.min.css') }}">
    <link rel="stylesheet" href="{{ asset('template/css/magnific-popup.css') }}">
    <link rel="stylesheet" href="{{ asset('template/css/owl.carousel.css') }}">
    <link rel="stylesheet" href="{{ asset('template/css/hexagons.min.css') }}">
    <link rel="stylesheet" href="{{ asset('template/css/nice-select.css') }}">
    <link rel="stylesheet" href="{{ asset('template/css/main.css') }}">
    <link rel="stylesheet" href="{{ asset('galery/theme/css/style.css') }}">
    <link rel="stylesheet" href="{{ asset('galery/gallery/style.css') }}">
    <link rel="stylesheet" href="{{ asset('galery/mobirise/css/mbr-additional.css') }}">
    <style>
    .topnav {
      overflow: hidden;
      background-color: transparent;
    }

    .topnav a {
      float: left;
      display: block;
      color: #000;
      text-align: center;
      padding: 14px 16px;
      text-decoration: none;
      font-size: 17px !important;
      font-weight: 500 !important;
    }

    .topnav a:hover {
      background-color: #ddd;
      color: black;
    }

    .topnav a.active {
      /*background-color: #4CAF50;*/
      /*color: white;*/
      border-bottom: 2px solid #4caf50;
    }

    .topnav #dropdown a.active {
      background-color: #fff !important;
      /*color: white !important;*/
      border-bottom: 2px solid #4caf50;
    }

    .topnav .icon {
        display: none;
    }
    .logohome{
        display: none;
    }
    @media screen and (max-width: 768px) {
      .topnav a:not(:first-child) {display: none;}
      .topnav a.icon {
        float: left;
        display: block;
      }
      .dropdown{
        display: none;
      }
      .logohome{
        float: right;
        display: block;
      }
    }

    @media screen and (max-width: 768px) {
      .topnav.responsive {position: relative;}
      .topnav.responsive .icon {
        position: absolute;
        left: 0;
        top: 0;
      }
      .topnav.responsive a {
        float: none;
        display: block;
        text-align: left;
      }
      .topnav.responsive a.home {
        visibility: hidden;
      }

      .topnav.responsive a.home2 {
        visibility: inherit;
        display: inherit !important;
      }
      .Whatsapp21{
        visibility: hidden;
      }
      .topnav.responsive .dropdown{
        float: none;
        display: block;
        text-align: left;
      }
    }
.dropbtn {
  background-color: transparent !important;
  color: #000 !important;
  padding: 14px 16px !important;
  font-size: 16px !important;
  border: none !important;
}

.dropdown {
  position: relative;
  /*display: inline-block;*/
}

.dropdown-content {
  display: none !important;
  position: initial !important;
  background-color: #f1f1f1 !important;
  min-width: 160px !important;
  box-shadow: 0px 8px 16px 0px rgba(0,0,0,0.2) !important;
  z-index: 1 !important;
  /*top: 55px !important;*/
}

.dropdown-content a {
  color: black !important;
  padding: 12px 16px !important;
  text-decoration: none !important;
  display: block !important;
  background: #fff;
}

.dropdown-content a:hover {background-color: #ddd !important;}

.dropdown:hover .dropdown-content {display: block !important;}

.dropdown:hover .dropbtn {background-color: #ddd !important;}
    </style>
</head>

<body>

    <!--================ Offcanvus Menu Area =================-->
    <!-- <div class="side_menu"> -->
<!--         <div class="logo">
            <a href="index.html">
                <img src="{{ asset('template/img/logo.png')}}" alt="">
            </a>
        </div> -->
        <!-- <ul class="list menu-left">
            <li>
                <a href="{{ url('/')}}">Home</a>
            </li>
            <li>
                <a href="{{ url('/About-Us') }}">About Us</a>
            </li>
            <li>
                <div class="dropdown">
                    <button type="button" class="dropdown-toggle" data-toggle="dropdown">
                        Service <i class="fas fa-caret-right" style="margin-left: 20px"></i>
                    </button>
                    <div class="dropdown-menu" style="transform: translate3d(10px, 40px, 0px) !important;">
                        <a class="dropdown-item" href="{{ url('/Service/Natural-Gas') }}">Natural Gas</a>
                        <a class="dropdown-item" href="{{ url('/Service/Electricity') }}">Electricity</a>
                        <a class="dropdown-item" href="{{ url('/Service/Water-Treatment') }}">Water Treatment</a>
                        <a class="dropdown-item" href="{{ url('/Service/Waster-Water-Treatment') }}">Waste Water Treatment</a>
                        <a class="dropdown-item" href="{{ url('/Service/High-Speed-Diesel') }}">High Speed Diesel</a>
                        <a class="dropdown-item" href="{{ url('/Service/Waste-To-Energy') }}">Waste To Energy</a>
                        <a class="dropdown-item" href="{{ url('/Service/Photovoltaic') }}">Photovoltaic</a>
                        <a class="dropdown-item" href="{{ url('/Service/Internet-Of-Things') }}">Internet Of Things (IoT)</a>
                    </div>
                </div>
            </li>
            <li>
                <a href="{{ url('/Client') }}">Client</a>
            </li>
            <li>
                <a href="{{ url('/Gallery') }}">Gallery</a>
            </li>
            <li>
                <a href="{{ url('/Career') }}">Career</a>
            </li>
           <li>
                <a href="{{ url('/Contact') }}">Contact</a>
            </li>
        </ul>

        <ul class="menu">
            <li>
                <a class="dropbtn" href="{{ url('/')}}">Home</a>
            </li>
            <li>
                <a class="dropbtn" href="{{ url('/About-Us') }}">About Us</a>
            </li>
            <li>
                 <div class="dropdown">
                  <a class="dropbtn">Service</a>
                    <div class="dropdown-content">
                        <a class="dropdown-item" href="{{ url('/Service/Natural-Gas') }}">Natural Gas</a>
                        <a class="dropdown-item" href="{{ url('/Service/Electricity') }}">Electricity</a>
                        <a class="dropdown-item" href="{{ url('/Service/Water-Treatment') }}">Water Treatment</a>
                        <a class="dropdown-item" href="{{ url('/Service/Waster-Water-Treatment') }}">Waste Water Treatment</a>
                        <a class="dropdown-item" href="{{ url('/Service/High-Speed-Diesel') }}">High Speed Diesel</a>
                        <a class="dropdown-item" href="{{ url('/Service/Waste-To-Energy') }}">Waste To Energy</a>
                        <a class="dropdown-item" href="{{ url('/Service/Photovoltaic') }}">Photovoltaic</a>
                        <a class="dropdown-item" href="{{ url('/Service/Internet-Of-Things') }}">Internet of Things (IoT)</a>
                  </div>
                </div>
            </li>
            <li>
                <a class="dropbtn" href="{{ url('/Client') }}">Client</a>
            </li>
            <li>
                <a class="dropbtn" href="{{ url('/Gallery') }}">Gallery</a>
            </li>
            <li>
                <a class="dropbtn" href="{{ url('/Career') }}">Career</a>
            </li>
           <li>
                <a class="dropbtn" href="{{ url('/Contact') }}">Contact</a>
            </li>
            <li class="sos">
                <a href=""><i class="fab fa-facebook-square fa-3x"></i></a>
                <a href=""><i class="fab fa-instagram fa-3x"></i></a>
                <a href=""><i class="fab fa-linkedin fa-3x"></i></a>
                <a href=""><i class="fab fa-youtube fa-3x"></i></a>
            </li>
        </ul>
    </div> -->
    <!--================ End Offcanvus Menu Area =================-->
    <div class="menubawah-hidden">
        <div class="container" style="max-width: 1240px">
            <div class="Whatsapp21">
                <img src="{{ asset('template/img/icn.jpeg')}}" style="height: 30px;margin-bottom: 14px;margin:5px;visibility: hidden;">
            </div>
            <div class="Whatsapp">
                <a href="https://api.whatsapp.com/send?phone=6281296400431" target="_blank"><img src="{{ asset('template/img/wa.png')}}" style="height: 30px;margin-bottom: 14px;margin:5px;"></a>
            </div>
        </div>
    </div>
    <!--================ Canvus Menu Area =================-->
<div class="canvus_menu" style="background: #fff !important;">
    <div class="container" style="max-width: 1240px">
    </div>
</div>
<div class="canvus_menu">
    <div class="container" style="max-width: 1240px">
<!--         <a href="{{ url('/')}}">
            <img src="{{ asset('template/img/logo wira_logo (1).jpg') }}" class="hide1" style="float: left; height: 50px;">
            <img src="{{ asset('template/img/logo wira_logotext(1).jpg') }}" class="hide2" style="float: left; height: 50px;">
        </a> -->
        <div class="topnav" id="myTopnav">
            <!-- <a href="{{ url('/')}}">Home</a> -->
            <a href="javascript:void(0);" class="icon" onclick="myFunction()">
                <i class="fa fa-bars"></i>
            </a>
            <div class="logohome">
                <a href="{{ url('/')}}">
                    <img src="{{ asset('template/img/icn.jpeg')}}" class="hide1" style="float: left; height: 35px;">
                    <!-- <img src="{{ asset('template/img/logo wira_logo (1).jpg') }}" class="hide1" style="float: left; height: 35px;"> -->
                    <!-- <img src="{{ asset('template/img/logo wira_logotext(1).jpg') }}" class="hide2" style="float: left; height: 35px;"> -->
                </a>
            </div>
            <a href="{{ url('/') }}" class="home">Home</a>
            <a href="{{ url('/') }}" class="home2" style="display: none;">Home</a>
            <a href="{{ url('/About-Us') }}">About Us</a>
            <a href="{{ url('/Gallery') }}">Gallery</a>
            <a href="{{ url('/Career') }}">Career</a>
            <a href="{{ url('/Contact') }}">Contact</a>
            <div class="dropdown" id="dropdown">
                <a class="dropbtn">Services</a>
                    <div class="dropdown-content">
                        <a class="dropdown-item" href="{{ url('/Service/Natural-Gas') }}">Natural Gas</a>
                        <a class="dropdown-item" href="{{ url('/Service/Electricity') }}">Electricity</a>
                        <a class="dropdown-item" href="{{ url('/Service/Water-Treatment') }}">Water Treatment</a>
                        <a class="dropdown-item" href="{{ url('/Service/Waster-Water-Treatment') }}">Waste Water Treatment</a>
                        <a class="dropdown-item" href="{{ url('/Service/High-Speed-Diesel') }}">High Speed Diesel</a>
                        <a class="dropdown-item" href="{{ url('/Service/Waste-To-Energy') }}">Waste To Energy</a>
                        <a class="dropdown-item" href="{{ url('/Service/Photovoltaic') }}">Photovoltaic</a>
                        <a class="dropdown-item" href="https://weiots.io" target="_blank">Internet of Things (IoT)</a>
                  </div>
            </div>

            <div class="Whatsapp21">
                <img src="{{ asset('template/img/icn.jpeg')}}" style="height: 35px;margin:2px;">
            </div>
        </div>
    </div>
</div>
<!--     <div class="canvus_menu">
        <div class="container" style="max-width: 1240px">
            <div class="icon">
                <a href="{{ url('/')}}">
                    <img src="{{ asset('template/img/logo wira_logo (1).jpg') }}" class="hide1" style="float: left;/*width: 5%*/ height: 50px;margin-top: 15px;">
                    <img src="{{ asset('template/img/logo wira_logotext(1).jpg') }}" class="hide2" style="float: left;/*width: 20%;*/ height: 50px;margin-top: 15px;">
                </a>
            </div>
            <div class="toggle_icon" title="Menu Bar">
                <span></span>
                <span class="after"></span>
                <span class="before"></span>
            </div>
        </div>
    </div> -->

    <!--================ End Canvus Menu Area =================-->

    <script src="{{ asset('template/js/vendor/jquery-2.2.4.min.js') }}"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.11.0/umd/popper.min.js" integrity="sha384-b/U6ypiBEHpOf/4+1nzFpr53nxSS+GLCkfwBdFNTxtclqqenISfwAzpKaMNFNmj4"
     crossorigin="anonymous"></script>
    <script src="{{ asset('template/js/vendor/bootstrap.min.js') }}"></script>
    <script src="{{ asset('template/js/jquery.ajaxchimp.min.js') }}"></script>
    <script src="{{ asset('template/js/jquery.magnific-popup.min.js') }}"></script>
    <script src="{{ asset('template/js/parallax.min.js') }}"></script>
    <script src="{{ asset('template/js/owl.carousel.min.js') }}"></script>
    <script src="{{ asset('template/js/jquery.nice-select.min.js') }}"></script>
    <script src="{{ asset('template/js/jquery.sticky.js') }}"></script>
    <script src="{{ asset('template/js/hexagons.min.js') }}"></script>
    <script src="{{ asset('template/js/jquery.counterup.min.js') }}"></script>
    <script src="{{ asset('template/js/waypoints.min.js') }}"></script>
    <script src="{{ asset('template/js/main.js') }}"></script>
    <script src="{{ asset('galery/masonry/masonry.pkgd.min.js') }}"></script>
    <script src="{{ asset('galery/imagesloaded/imagesloaded.pkgd.min.js') }}"></script>
    <script src="{{ asset('galery/theme/js/script.js') }}"></script>
    <script src="{{ asset('galery/gallery/script.js') }}"></script>
    <script>
        $(window).scroll(function() {

            var scroll = $(window).scrollTop();

            if (scroll >= 100) {
                $(".menubawah-hidden").addClass("menubawah");
                $(".canvus_menu .toggle_icon span").css("background","#fff");
                $(".canvus_menu .icon .hide2").css("visibility","hidden");
                // $(".canvus_menu").css("background","transparent");
                // $(".canvus_menu .icon").img("url""{{ asset('template/img/wa.png')}}");
            } else {
                $(".menubawah-hidden").removeClass("menubawah");
                $(".canvus_menu .toggle_icon span").css("background","#222222");
                $(".canvus_menu .icon .hide2").css("visibility","inherit");
                // $(".canvus_menu").css("background","transparent");

            }
        });

        function myFunction() {
          var x = document.getElementById("myTopnav");
          if (x.className === "topnav") {
            x.className += " responsive";
          } else {
            x.className = "topnav";
          }
        }

        $(function(){

            var url = window.location.pathname,
                urlRegExp = new RegExp(url.replace(/\/$/,'') + "$"); // create regexp to match current url pathname and remove trailing slash if present as it could collide with the link in navigation in case trailing slash wasn't present there
                // now grab every link from the navigation
                $('.topnav a').each(function(){
                    // and test its normalized href against the url pathname regexp
                    if(urlRegExp.test(this.href.replace(/\/$/,''))){
                        $(this).addClass('active');
                    }
                });

        });
        // $(window).scroll(function() {

        //     var scroll = $(window).scrollTop();

        //     if (scroll >= 200) {
        //         $('.canvus_menu .toggle_icon span').css('background','#fff');
        //     } else {
        //         $('.canvus_menu .toggle_icon span').css('background','#222222');
        //     }
        // });

        // $(window).scroll(function() {

        //     var scroll = $(window).scrollTop();

        //     if (scroll >= 200) {
        //         $('.canvus_menu .toggle_icon span::before').css('background','#fff');
        //     } else {
        //         $('.canvus_menu .toggle_icon span::before').css('background','#222222');
        //     }
        // });

        // $(window).scroll(function() {

        //     var scroll = $(window).scrollTop();

        //     if (scroll >= 200) {
        //         $(".canvus_menu .toggle_icon span:after").css("background","#fff");
        //     } else {
        //         $(".canvus_menu .toggle_icon span:after").css("background","#222222")
        //     }
        // });
    </script>
</body>
</html>
