<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/
/*home*/
Route::get('/', function () {
    return view('Home');
});
Route::post('/sendEmail', 'Email@sendEmail');
// Route::get('/contactJob', 'Applyjob@getcontactJob');
// Route::post('/contactJob', 'Applyjob@postcontactJob');
// Route::post('/Applyjob', 'Email@Applyjob');
// Route::get('/Natural-Gas', function () {
//     return view('naturalgas');
// });


/*about us*/
Route::get('/About-Us', function () {
    return view('about');
});

/*Service*/
Route::get('/Service/Natural-Gas', function () {
    return view('service/naturalgas');
});

Route::get('/Service/Electricity', function () {
    return view('service/electricity');
});

Route::get('/Service/Water-Treatment', function () {
    return view('service/water-treatment');
});

Route::get('/Service/Waster-Water-Treatment', function () {
    return view('service/waster-water-treatment');
});

Route::get('/Service/High-Speed-Diesel', function () {
    return view('service/high-speed-diesel');
});

Route::get('/Service/Waste-To-Energy', function () {
    return view('service/waste-to-energy');
});

Route::get('/Service/Photovoltaic', function () {
    return view('service/photovoltaic');
});

Route::get('/Service/Internet-Of-Things', function () {
    return view('service/iot');
});

/*client*/
Route::get('/Client', function () {
    return view('client');
});

/*Galery*/
Route::get('/Gallery', function () {
    return view('galery');
});

/*Career*/
Route::get('/Career', function () {
    return view('job/index');
});


Route::get('/Sales-Executive', function () {
    return view('job/sales');
});
Route::post('/Applyjob', 'Applyjob@Applyjob');


Route::get('/Engineer', function () {
    return view('job/enginer');
});

Route::get('/Architect', function () {
    return view('job/Architect');
});

Route::get('/Sustainable-Engineer', function () {
    return view('job/Sustainable');
});

/*Contact*/
Route::get('/Contact', function () {
    return view('contact');
});

Route::get('/Email', function () {
    return view('email');
});

//email
// Route::get('/email', function () {
//     return view('send_email');
// });
// Route::post('/sendEmail', 'Email@sendEmail');